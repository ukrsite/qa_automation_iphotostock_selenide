package ua.com.ukrsite;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.testng.annotations.Test;
import ua.com.ukrsite.Pages.BoardPage;
import ua.com.ukrsite.Pages.LoginPage;
import ua.com.ukrsite.Pages.MainPage;

import static com.codeborne.selenide.Selenide.open;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.time;

public class IStockPhotoTest {

    @Test
    public void testIStockPhoto() throws InterruptedException {

        Configuration.browser="Chrome";
        String newBoardName = "new Board " + time();

        LoginPage loginPage = open("https://www.istockphoto.com/",LoginPage.class);
        MainPage mainPage = loginPage.signIn("ukrsite@ukr.net","Gfhjkm2018");
        BoardPage boardPage = mainPage.addNewBoard(newBoardName);
        boardPage.addPhotoToBoard(newBoardName);
        boardPage.removePhotoFromBoard();
        boardPage.removeBoard();


    }
}
