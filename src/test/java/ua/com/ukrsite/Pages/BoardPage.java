package ua.com.ukrsite.Pages;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static com.codeborne.selenide.Selenide.switchTo;

public class BoardPage {

    public BoardPage addPhotoToBoard(String boardName) {

        $("li > a[data-nav='nav=nav_Photos']").click();
        $("figcaption").click();
        $("span.checkbox > label").click();
        $("button.actions-modal-button.ng-scope").click();
        $("div.option-container > h2").click();     //11111
        $(byText(boardName)).click();
        return page(BoardPage.class);
    }

    public BoardPage removePhotoFromBoard() {

        $("span.checkbox > label").click();
        $("button.outline.clear-all").click();

        return page(BoardPage.class);
    }

    public BoardPage removeBoard() {

        $("a.delete-board.ng-scope").click();
        //Thread.sleep(4000);
        switchTo().alert().accept();

        return page(BoardPage.class);
    }
}
