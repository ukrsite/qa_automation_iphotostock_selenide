package ua.com.ukrsite.Pages;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class MainPage {

    public BoardPage addNewBoard(String boardName) {

        $("#open_board").click();
        $("a.board-link.create-board-link").shouldBe(visible).click();

        $(byName("boardname")).setValue(boardName);
        $("form[name='form'] > a.button").click();

        return page(BoardPage.class);
    }
}
