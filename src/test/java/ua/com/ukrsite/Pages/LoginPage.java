package ua.com.ukrsite.Pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;

public class LoginPage {
    public MainPage signIn(String login, String psw) {

        $(("a[class='account']")).click();
        $("input[id='new_session_username']").setValue(login);
        $("#new_session_password").setValue(psw);
        $("#sign_in").click();

        return page(MainPage.class);
    }
}
